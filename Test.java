import java.util.Scanner;
//import keyword is used to import built-in and user-defined packages into your java source file
// java.util is a java utility library
public class Test {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		//Scanner class is used to read input from keyboard in Java
		// scanner is a class, creating object(sc) of scanner 
		//System.in is a standard input stream
		// It is used to read the input from the keyboard
		System.out.println("enter the subject name");
		String name=sc.nextLine();
		//nextLine() is a method of scanner class
		//it moves the scanner position to the next line and returns the value as a string.
		System.out.println("enter the marks");
		int marks=sc.nextInt();
		//it scans the next token as an int value.
		System.out.println("subject is " + name +" "+ "mark is"+" "+ marks);
		

	}

}
